# tribute-to-Ralph-Modejski
🌉 Tribute Page 🌉

Ralph Modejski tribute page (Website Design & Development)

## About
I decided make my own project - tribute page. I chose Ralph Modjeski for this tribute page because i want to show the story of master engineer Ralph Modjeski besides learning new technology.

## Final result of the project
[Page](https://darekrepos.github.io/tribute-to-Ralph-Modejski/) - Final result of the project

## Feedback
I love to receive feedback on improving my site, so please report a problem if you find something.
